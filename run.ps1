# Check if java is available
if (-not (Test-Path -Path "java.exe")) {
    Write-Host "Java is not installed or not in the system's PATH. Please install Java and try again."
    exit 1
}

# Run the Tetris game
java -jar tetris.jar -s 20 10
