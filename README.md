<img src="demo.gif"  height="800">  

# TetrisAI
A Tetris clone and an AI that uses backtracking to find all possible placements for a shape and heuristics and a weighted linear function to evaluate solution candidates.  

## About
This was a passion project that I worked on during undergrad instead of doing my assignments. I used it to test out new programming concepts as I learned them and as a result the code is a mess in many places. It is presented as is.

## Instructions
Clone the repository and run either `run.sh` or `run.ps1`
Try `run-landscape.sh` for a demonstration of alternate board layouts.  
See [help.txt](help.txt) for more options.

## Demo
[Full demonstration video](demo.mp4)  


