package ai.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import ai.state.GameState;

public class SolutionValue {
	
	private boolean configLoaded = false;	
	private final static Logger LOGGER = Logger.getLogger(SolutionValue.class.getName());
	private Weights weights = null;
	
	private InternalGameBoard gBWithShape, gBWithoutShape, shapeCoords;
	
	static {
		LOGGER.setLevel(Level.OFF);	
		//loadConfiguration(null);
		//LOGGER.setLevel(Level.SEVERE);	
	}
	public SolutionValue () {
		this.weights = Weights.getDefault();
		loadConfiguration("");
	}
	public SolutionValue (Weights weights) {
		this.weights = weights;
		loadConfiguration("");
	}
	protected void loadConfiguration(String configFile) {		// loads the weightings into the decision function, to be used for rapid/automated algorithm improvement
	}
	public double calculateSolution(GameState inState, ComputedValue values) {
		return calculateSolution(values);
	}

	public double calculateSolution(ComputedValue values) {
		if (values == null) return Double.NEGATIVE_INFINITY;
		double val = 0d;
		
		val += this.weights.AC*values.adjacentCount; 
		val -= this.weights.MB*values.missingBelow;
		val += this.weights.EC*values.edgeCount;
		val += this.weights.RC*values.numRowsThatWillClear;
		val -= this.weights.BF*values.buriedFactor; 
		val += this.weights.DG*values.distanceFromBottom;
		val -= this.weights.BG*values.badGapBesideShape; 
		val += this.weights.CF*values.coveredFactor;
		val += this.weights.PS*values.proximityToSpawn;
		val += this.weights.SC*values.shapeClear;
		
		return val;
	}
	private double getDensity (GameState inState) {
		int [][] gB = inState.getBoardWithCurrentShape().getState();
		
		int totalSpacesFilled = 0;
		int totalSpacesChecked = 0;
		for (int row=0;row<gB.length; row++) {
			int numFilled = 0;  // Per row
			for (int col=0;col<gB[row].length;col++) {
				if (gB[row][col] != 0) numFilled++;
			}
			
			if (numFilled > 0) {
				totalSpacesFilled += numFilled;
				totalSpacesChecked += gB[row].length; 
			}
			
		}
		return totalSpacesFilled / ((double) totalSpacesChecked); 
	}
	public ComputedValue getSolutionParameters(GameState inState) {
		gBWithShape = new InternalGameBoard (inState.getBoardWithCurrentShape().getState());
		gBWithoutShape = new InternalGameBoard (inState.getBoardWithoutCurrentShape().getState());
		shapeCoords = new InternalGameBoard (inState.getShape().getCoords());
		
		ComputedValue values = new ComputedValue ();
		if (this.weights.AC != 0d) values.adjacentCount = getAdjacentCount(inState);
		if (this.weights.BF != 0d) values.buriedFactor  = getBurriedCount(inState);
		if (this.weights.CF != 0d) values.coveredFactor = getCoveredFactor (inState);
		if (this.weights.DG != 0d) values.distanceFromBottom = getAvgDistanceFromBottom (inState);
		if (this.weights.BG != 0d) values.badGapBesideShape = getBadGapBesideShape();
		if (this.weights.MB != 0d) values.missingBelow = getMissingBelow (inState);
		if (this.weights.EC != 0d) values.edgeCount = getEdgeCount (inState);
		if (this.weights.DS != 0d) values.density = getDensity(inState);
		if (this.weights.RC != 0d) values.numRowsThatWillClear = getNumRowsThatWillClear(inState);
		if (this.weights.PS != 0d) values.proximityToSpawn = getProximityToSpawn(inState);
		if (this.weights.SC != 0d) values.shapeClear = getShapeClear(inState);
		
		return values;
	}
	public double getProximityToSpawn (GameState inState) {
		/* Accounts for engine variability in 
		 * how the engine centers a shape with odd number of columns
		 */
		final int nCols = Math.max(inState.numColumns(),1);
		int spawnColStart = Math.max((nCols / 2) - 2,0); 
		int spawnColEnd = Math.min((nCols / 2) + 1, nCols-1);
		int spawnRowStart = 0;
		int spawnRowEnd = 1;
		
		double minProximity = Double.POSITIVE_INFINITY;
		
		for (int [] coord: inState.getShape().getCoords()) {
			double colDiff = 0d;
			if (coord[1] < spawnColStart) colDiff = spawnColStart - coord[1];
			else if (coord[1] > spawnColEnd) colDiff = coord[1] - spawnColEnd;
			else colDiff = Math.abs((nCols/2) - coord[1]);
			
			double rowDiff = 0d;
			if (coord[0] > spawnRowEnd) rowDiff = coord[0] - spawnRowEnd;
			else if (coord[0] < spawnRowStart) rowDiff = spawnRowStart - coord[0];
			else rowDiff = Math.abs((spawnRowEnd - spawnRowStart) - coord[0]);
			
			
			double thisProximity = Math.sqrt(colDiff*colDiff + rowDiff*rowDiff);
			minProximity = Math.min(minProximity,  thisProximity);
		}
		return minProximity;
	}
	private double packingValue (GameState inState) {
		double factor = 0d;
		final int nCols = inState.numColumns();
		final int nRows = inState.numRows();
		
		int [][] gBWith = inState.getBoardWithCurrentShape().getState();
		int [][] gBWOut = inState.getBoardWithoutCurrentShape().getState();
		
		int [] rowFillBef = new int [nRows];
		int [] rowFillAft = new int [nRows];
		
		for (int row=0;row<nRows;row++) {
			for (int col=0;col<nCols;col++) {
				if (gBWith[row][col] != 0) rowFillAft[row]++;
				if (gBWOut[row][col] != 0) rowFillBef[row]++;
			}
			
			if (rowFillBef[row] == 0 && rowFillAft[row] != 0) factor+=rowFillAft[row];
		}
		return factor;
	}
	private int getNumRowsThatWillClear (GameState inState) {
		int [][] gB = inState.getBoardWithCurrentShape().getState();
		int clearCount = 0;
		final int nRows = inState.numRows();
		final int nCols = inState.numColumns();
		for (int row=0;row<nRows;row++) {
			int rowCount = 0;
			for (int col=0;col<nCols;col++) {
				if (gB[row][col] != 0) rowCount ++;
			}
			if (rowCount == nCols) clearCount++;
		}
		return clearCount;
	}
	private int getLowestShapePoint (GameState inState) {
		int lowestRow = 0;
		for (int [] coord: inState.getShape().getCoords()) {
			lowestRow = Math.max(lowestRow,  coord[0]);
		}
		return lowestRow;
	}
	private int getAvgDistanceFromBottom(GameState inState) {
		int avg = 0;
		int count = 0;
		for (int [] coord: inState.getShape().getCoords()) {
			avg += coord[0];
			count ++;
		}	
		return avg / count;
	}
	/**
	 * A measure of the number of missing spaces below the shape before row clear
	 * @param inState
	 * @return
	 */
	private int getMissingBelow (GameState inState) {
		int [][] gB = inState.getBoardWithCurrentShape().getState();
		int missingBelow = 0;

		for (int [] coord : inState.getShape().getCoords()) {
			if (inBounds(coord[0] + 1, coord[1], inState) && gB[coord[0] + 1][coord[1]] == 0) {
				missingBelow++;
			}
		}
		return missingBelow;
	}
	private boolean inBounds (final int r, final int c, final GameState gState) {
		int maxRows = gState.numRows();
		int maxCols = gState.numColumns();
		if (r >= maxRows || c >= maxCols || (r | c) < 0) return false;
		return true;
	}
	private int getCoveredFactor (GameState inState) {
		int coveredCount = 0;
		int [][] gB = inState.getBoardWithoutCurrentShape().getState();
		for (int [] coord : inState.getShape().getCoords()) {
			if (inBounds (coord[0]-1,coord[1],inState) && gB[coord[0]-1][coord[1]] != 0) {
				coveredCount ++;
			}
		}
		return coveredCount;
	}
	private int getEdgeCount (GameState inState) {
		int count = 0;
		final int maxCol = inState.numColumns() - 1; // Cause array indexes
		final int maxRow = inState.numRows() - 1;
		for (int [] coord : inState.getShape().getCoords()) {
			if (coord[0] == maxRow || coord[0] == 0) count++;
			if (coord[1] == 0 || coord[1] == maxCol) count++; 
		}
		return count;
	}

	private int getBurriedCount (GameState inState) {
		int gB [][] = inState.getBoardWithCurrentShape().getState();
		int burriedFactor = 0;
		for (int [] coord : inState.getShape().getCoords()) {
			for (int row=coord[0]; row<inState.numRows(); row++) {
				if (inBounds (row,coord[1],inState) && gB[row][coord[1]] == 0) {
					burriedFactor += 1;
				}
			}
		}
		return burriedFactor;
	}
	private int getRoughness (GameState inState) {
		return getRoughness (inState, 0, inState.getBoardWithoutCurrentShape().getState()[0].length);
	}
	private int getShapeClear (GameState inState) {
		int [] clearedRows = this.getRowsThatWillClear(inState);
		int shapeClear = 0;
		for (int [] coord : inState.getShape().getCoords()) {
			if (clearedRows[coord[0]] == 1) shapeClear += 1;
		}
		return shapeClear;
	}
	private int getAdjacentCount (GameState inState) {
		int count = 0;
		int [][] gB = inState.getBoardWithoutCurrentShape().getState();
		for (int [] coord : inState.getShape().getCoords()) {
			if (inBounds(coord[0]+1,coord[1],inState) && gB[coord[0]+1][coord[1]] != 0) {
				count++;
			}
			if (inBounds(coord[0]-1,coord[1],inState) && gB[coord[0]-1][coord[1]] != 0) {
				count++;
			}
			if (inBounds(coord[0],coord[1]+1,inState) && gB[coord[0]][coord[1]+1] != 0) {
				count++;
			}
			if (inBounds(coord[0],coord[1]-1,inState) && gB[coord[0]][coord[1]-1] != 0) {
				count++;
			}
		}
		return count;
	}
	private int [] getRowsThatWillClear (GameState gState) {
		//System.out.println("GameBoard Size: " + Arrays.toString(gState.getBoardSize()));
		//System.out.println ("AS Rows cleard saw it: " + GameState.dumpState(gState, false));
		int [] rowsThatWillClear = new int [gState.getBoardSize()[0]];
		for (int i=0;i<rowsThatWillClear.length;i++) {
			//System.out.println ("ROW: " + i + Arrays.toString(gState.getBoardWithCurrentShape().getState()[i]));
			rowsThatWillClear [i] = 1;
			for (int j=0;j<gState.getBoardSize()[1];j++) {
				if (gState.getBoardWithCurrentShape().getState()[i][j] == 0) {
					rowsThatWillClear [i] = 0;
					break;
				}
			}
		}
		return rowsThatWillClear;
	}
	private int getBadGapBesideShape () {
		int numBadGaps = 0;
		int rows = gBWithShape.getRows();
		int cols = gBWithShape.getCols();
		
		
		int countSinceLast = 0;
		for (int r=0;r<rows;r++) {
			for (int c=0;c<cols;c++) {
				if (gBWithShape.get(r, c) == 0) countSinceLast++;
				else {
					if (countSinceLast == 1) numBadGaps++;
					countSinceLast = 0;
				}
			}
			if (countSinceLast == 1) numBadGaps++;
			countSinceLast = 0;
		}
		return numBadGaps;
	}
	private int getRoughness (GameState inState, int startColumn, int endColumn) {
		int oldDepth = getDepthOfColumn (inState.getBoardWithCurrentShape().getState(), 0);
		int roughness = 0;
		final int maxCol = endColumn;
		return 0;
	}
	private int getDepthOfColumn (int [][] gameBoard, int column) {
		for (int r=0;r<gameBoard.length;r++) {				
			if (gameBoard[r][column] != 0) {
				return r;
			}
		}
		return gameBoard.length;
	}
	private class InternalGameBoard {
		final int [] board;
		final int rows, cols;
		public InternalGameBoard (int [][] sqBoard) {
			rows = sqBoard.length;
			cols = sqBoard[0].length;
			
			board = new int [rows*cols];
			
			int r,c;
			for (int i=0;i<board.length;i++) {
				r = i / cols;
				c = i % cols;
				
				board[i] = sqBoard[r][c];
			}
		}
		
		public final int getRows () {
			return rows;
		}
		public final int getCols () {
			return cols;
		}
		public final int getWithCheck (final int r, final int c) {
			if (r >= rows || c >= cols || (r | c) < 0) {
				return -1;
			} else {
				return get (r, c);
			}
		}
		public final int get (final int r, final int c) {
			return board[r*cols + c];
		}
	}
}
