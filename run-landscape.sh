#!/bin/bash

# Check if java is available
if ! command -v java &> /dev/null; then
    echo "Java is not installed or not in the system's PATH. Please install Java and try again."
    exit 1
fi

cat help.txt

# Run the Tetris game
java -jar tetris.jar -s 10 20
